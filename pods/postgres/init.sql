CREATE DATABASE bistox;

CREATE USER bistox WITH password 'bistox';

GRANT ALL ON DATABASE bistox TO bistox;
