#!/bin/bash -ex

cd "$(dirname "$0")"

not_exists() {
    [ ! -f "$1" ]
    return $?
}

if not_exists secret/docker.yaml; then
    echo "Gitlab registry secret key is not found [docker.yaml]"
    exit -1
fi
if not_exists secret/dokube-bistox-1.yaml; then
    echo "DO kubernetes secret key is not found [dokube-bistox-1.yaml]"
    exit -1
fi

# init
export KUBECONFIG=secret/dokube-bistox-1.yaml
kubectl version 2>&1 &>/dev/null || (echo kubernetes is not found 1>&2 && exit 1)
kubectl apply -f secret/docker.yaml


pods=(redis postgres ethereum-node bitcoin-node)

for svc in ${pods[@]}; do
    if not_exists pods/$svc/Dockerfile; then
        echo "WARN: Dockerfile not found!"
        continue
    fi

    docker build -t registry.gitlab.com/bistox/bistox_deployment/$svc:latest pods/$svc --network=host
    docker push registry.gitlab.com/bistox/bistox_deployment/$svc:latest
    kubectl apply -f pods/$svc/kube_deployment.yaml
    kubectl apply -f pods/$svc/kube_service.yaml
done


bistox_pods=(bistox-market bistox_api bistox_engine bistox_payserver)

for svc in ${bistox_pods[@]}; do
    if [ -f ../$svc/k8s/kube_config.yaml ]; then
        kubectl apply -f ../$svc/k8s/kube_config.yaml
    fi
    kubectl apply -f ../$svc/k8s/kube_deployment.yaml
    kubectl apply -f ../$svc/k8s/kube_service.yaml
done

kubectl apply -f kube_balancer_service.yaml

echo Done!
